#!/usr/bin/env python3
from serial import *
import requests
import json
import datetime
import sys
import time
print('************************')
print('*                      *')
print('*       Bab\'App       *')
print('*                      *')
print('************************')
print('')
print('Bab Starting')
time.sleep(10)
print('Bab Ready')


URL = 'http://9adc8f44-25aa-4c2f-bf0a-cd979aca9d73.pub.cloud.scaleway.com/game'
game = None
BAB = 'BabyFoot hall 1'
BABID = 'b1c831a6-249f-4af7-b399-9f26f43594cf'
blue = 0
red = 0


def post(url, json):
    return requests.post(url, json)

def get(url):
    return requests.get(url).json()

def patch(url, json):
    return requests.patch(url, json)

def findAction(ligne):
    global game
    global blue
    global red
    if ligne == 'startGame':
        post(url = URL, json = {
            'bab_id': BABID,
            'scorered': 0,
            'scoreblue': 0
        })
        res = get(url = URL+'?stoppedat=is.null&bab_id=eq.'+BABID)
        game = res[0]
        print(game)
        red = 0
        blue = 0
        print(game['id'])
    elif ligne == 'stopGame' and game != None:
        print(datetime.datetime.now().time())
        req = patch(url = (URL+'?id=eq.'+game['id']), json = {
            'stoppedat': datetime.datetime.now()
        })
        print(req.status_code)
        print(req.text)
        blue = 0
        red = 0
        game = None
    elif 'red' in ligne or 'blue' in ligne:
        print(game)
        data = json.loads(ligne)
        if 'red' in ligne:
            red = data['red']
        elif 'blue' in ligne:
            blue = data['blue']
        print(blue)
        print(red)
        req = patch(url = (URL+'?id=eq.'+game['id']), json = {
            'scoreblue': blue,
            'scorered': red
        })
        print(req.status_code)
        print(req.text)



arduino = Serial(port="/dev/ttyACM0", baudrate=9600, timeout=1, writeTimeout=1)
if arduino.isOpen():
    while True:
        try:
            ligne = arduino.readline()
            row = ligne[:-2].decode()
            findAction(row)
        except:
            print('Unexpected error:', sys.exc_info()[0])
